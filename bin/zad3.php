<?php

chdir(__DIR__ .'/../');
require('vendor/autoload.php');

if(empty($argv[1]) || $argv[1] === 'help') {
	echo 'Uzycie: php bin/zad3.php [nazwa pliku] [k]'. PHP_EOL;
	echo ' gdzie:'. PHP_EOL;
	echo '  nazwa pliku zawiera sciezke do pliku CSV z ID uzytkownikow i odsluchanych przez nich utworow'. PHP_EOL;
	echo '  k to liczba funkcji hashujacych wykorzystywanych przez filtr Blooma (domyslnie 2)'. PHP_EOL;
	return;
}

echo 'Rozpoczeto wczytywanie pliku '. $argv[1] . PHP_EOL;
$startTime = microtime(true);
$loader = new Quis\Zad3\CSVLoader();
list($usersSet, $songsSet) = $loader->load($argv[1]);
$endTime = microtime(true);
$processingTime = round($endTime-$startTime, 3);
echo "Czas wczytywania pliku: $processingTime sekund". PHP_EOL;

$k = isset($argv[2]) ? intval($argv[2]) : 2;

echo 'Liczba uzytkownikow: '. $usersSet->count() . PHP_EOL;

echo 'Rozpoczeto obliczanie tablicy pomylek...'. PHP_EOL;
$board = new Quis\Zad2\BoardOfMistakes($usersSet->count(), $usersSet->count()*100);

$songsCount = 100;
$allUsers = $usersSet->getAll();
$maxSongId = $songsSet->getMaxId();

$board->setStartTime();
foreach($allUsers as $user) {
	if(!$user->getSongsSet()->count()) {
		echo 'Brak utworow (user id '. $user->getId() .'), pomijam...'. PHP_EOL;
		continue;
	}
	
	$referenceFilter = new Quis\Zad2\ReferenceFilter();
	$bloomFilter = new Quis\Zad2\BloomFilter($k, 100, $maxSongId); //TODO m
	
	$userSongs = $user->getSongs();
	foreach($userSongs as $song) {
		$songId = $song->getId();
		$referenceFilter->add($songId);
		$bloomFilter->add($songId);
	}

	$board->iterate($bloomFilter, $referenceFilter, new \ArrayIterator($songsSet->getRandomElementsIds($songsCount)));
}
$board->setEndTime();
$board->output();


