<?php

/*
	szybkie komendy uruchamiające:
		php bin/zad1.php division data/facts-nns.csv
		php bin/zad1.php universal data/facts-nns.csv
		php bin/zad1.php division
		php bin/zad1.php universal
*/

chdir(__DIR__ .'/../');
require('vendor/autoload.php');

$mValues = [10, 1000, 100000];

$method = 'division';
if(isset($argv[1])) {
	if($argv[1] === 'help') {
		echo 'Uzycie: php bin/zad1.php [hash] [plik]'. PHP_EOL;
		echo ' gdzie:'. PHP_EOL;
		echo '  hash to metoda hashowania danych, przyjmuje wartosci division (proste dzielenie) lub universal (domyslnie division)'. PHP_EOL;
		echo '  plik to lokalizacja pliku z danymi CSV (jesli plik nie zostanie podany, skrypt uruchomi sie dla wygenerowanych danych syntetycznych)'. PHP_EOL;
		return;
	}
	$method = $argv[1];
}

if($argc === 1) {
	echo 'Uzycie: php bin/zad1.php [metoda division/universal] [plik z danymi]' . PHP_EOL;
}

if(empty($argv[2])) {
	switch($method) {
		case 'universal':
			echo 'Uruchamiam UniversalHashing'. PHP_EOL;
			
			foreach($mValues as $m)
			{	
				$max = pow(10,8) - 1;
				$obj = new Quis\Zad1\SyntheticExperiment();
				$obj->setHashM($m);
				$obj->setMaximum($max);

				echo 'Uruchamiam dla wszystkich liczb od 0 do 10^8 - 1'. PHP_EOL;
				$func = new Quis\Zad1\UniversalHashing($m, $max);
				$obj->run($func, ['type' => Quis\Zad1\SyntheticExperiment::TYPE_ALL]);
				
				echo 'Uruchamiam dla liczb parzystych od 0 do 2 * 10^8 - 1'. PHP_EOL;
				$func = new Quis\Zad1\UniversalHashing($m, 2*$max);
				$obj->run($func, ['type' => Quis\Zad1\SyntheticExperiment::TYPE_EVEN]);
				
				echo 'Uruchamiam dla liczb nieparzystych od 0 do 2 * 10^8 - 1'. PHP_EOL;
				$func = new Quis\Zad1\UniversalHashing($m, 2*$max);
				$obj->run($func, ['type' => Quis\Zad1\SyntheticExperiment::TYPE_ODD]);
			}
			break;
		case 'division':
			echo 'Uruchamiam DivisionHashing'. PHP_EOL;
			foreach($mValues as $m)
			{	
				$max = pow(10,8) - 1;
				$obj = new Quis\Zad1\SyntheticExperiment();
				$obj->setHashM($m);
				$obj->setMaximum($max);

				echo 'Uruchamiam dla wszystkich liczb od 0 do 10^8 - 1'. PHP_EOL;
				$func = new Quis\Zad1\DivisionHashing($m, $max);
				$obj->run($func, ['type' => Quis\Zad1\SyntheticExperiment::TYPE_ALL]);
				
				echo 'Uruchamiam dla liczb parzystych od 0 do 2 * 10^8 - 1'. PHP_EOL;
				$func = new Quis\Zad1\DivisionHashing($m, 2*$max);
				$obj->run($func, ['type' => Quis\Zad1\SyntheticExperiment::TYPE_EVEN]);
				
				echo 'Uruchamiam dla liczb nieparzystych od 0 do 2 * 10^8 - 1'. PHP_EOL;
				$func = new Quis\Zad1\DivisionHashing($m, 2*$max);
				$obj->run($func, ['type' => Quis\Zad1\SyntheticExperiment::TYPE_ODD]);
			}
			break;
		default:
			echo 'Nieznana metoda ' . $method . ', uzyj metody division lub universal' . PHP_EOL;
	}
} else {
	try {
		echo 'Rozpoczynam wczytywac dane z pliku '. $argv[2] . PHP_EOL;
		$loader = new Quis\Zad1\CSVLoader();
		$data = $loader->load($argv[2]);
		
		switch($method) {
			case 'universal':
				echo 'Uruchamiam UniversalHashing dla danych z pliku '. $argv[2] . PHP_EOL;
				foreach($mValues as $m)
				{	
					$obj = new Quis\Zad1\DataExperiment();
					$obj->setHashM($m);
					$obj->setMaximum(332124); // TODO
					//$obj->calculateMaximum($data);
					
					$func = new Quis\Zad1\UniversalHashing($m, $obj->getMaximum());
					$obj->run($func, ['data' => $data]);
				}
				break;
			case 'division':
				echo 'Uruchamiam DivisionHashing dla danych z pliku '. $argv[2] . PHP_EOL;
				foreach($mValues as $m)
				{	
					$obj = new Quis\Zad1\DataExperiment();
					$obj->setHashM($m);
					$obj->calculateMaximum($data);
					
					$func = new Quis\Zad1\DivisionHashing($m, $obj->getMaximum());
					$obj->run($func, ['data' => $data]);
				}
				break;
			default:
				echo 'Nieznana metoda ' . $method . ', uzyj metody division lub universal' . PHP_EOL;
		}
	} catch (Quis\Zad1\FileLoadingException $ex) {
		echo 'Wystapil blad: '. $ex->getMessage() . PHP_EOL;
	}	
}