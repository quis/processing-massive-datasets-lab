<?php

chdir(__DIR__ .'/../');
require('vendor/autoload.php');

if(isset($argv[1]) && $argv[1] === 'help') {
	echo 'Uzycie: php bin/zad2.php [n] [r] [k] [p]'. PHP_EOL;
	echo ' gdzie:'. PHP_EOL;
	echo '  n to liczebnosc zbioru danych (domyslnie 1 000 000)'. PHP_EOL;
	echo '  r to maksymalna liczba w zbiorze danych (domyslnie 100 000 000)'. PHP_EOL;
	echo '  k to liczebnosc funkcji hashujacych w filtrze Blooma (domyslnie 3)'. PHP_EOL;
	echo '  p to mnoznik dla n, n+(p*n) jest wykorzystywane jako m (dzielnik przy obliczaniu reszty z dzielenia) w UniversalHashing (domyslnie 50, czyli m jest wieksze od n o 50 razy, m nie moze byc wieksze od r)'. PHP_EOL;
	return;
}

$n = isset($argv[1]) ? intval($argv[1]) : 1000000;
$r = isset($argv[2]) ? intval($argv[2]) : 100000000;

if($r < $n) {
	throw new \Exception("Wartosc r nie powinna byc mniejsza od n: braknie liczb zeby zapelnic caly zbior");
}
$nStage = intval(0.05*$n);
$rStage = intval(0.02*$r);

echo 'Inicjalizacja filtrow'. PHP_EOL;
$set = new Quis\Zad2\ReferenceFilter();
echo 'Filtr referencyjny utworzony'. PHP_EOL;
$p = (isset($argv[4]) ? floatval($argv[4]) : 50);
//$m = intval($n+($p*$n));
$m = 10*$n; // TODO
if($m > $r) {
	throw new \Exception("Wartosc m nie moze byc wieksza od r! Zobacz php bin/zad2.php help");
}
$bloom = new Quis\Zad2\BloomFilter((isset($argv[3]) ? intval($argv[3]) : 3), $m, $r);
echo 'Filtr Blooma utworzony' . PHP_EOL;

$stage = 0;
$size = 0;
$startTime = microtime(true);
while($size < $n) {
	$num = rand(0, $r);
	$set->add($num);
	$bloom->add($num);
	$stage++;
	$size = $set->size();
	
	if($stage >= $nStage) {
		$stage = 0;
		echo 'Generowanie zbiorow danych... ('. round((100*$size)/$n, 2) .' %)'. PHP_EOL;
	}
}
$endTime = microtime(true);
$processingTime = round($endTime-$startTime, 3);
echo "Czas generowania danych: $processingTime sekund". PHP_EOL;

$iterator = new Quis\Zad2\RangeIterator($r);
$board = new Quis\Zad2\BoardOfMistakes($rStage, $r);
$board->setStartTime();
$board->iterate($bloom, $set, $iterator);
$board->setEndTime();
$board->output();