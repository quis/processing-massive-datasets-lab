<?php

chdir(__DIR__ .'/../');
require('vendor/autoload.php');

if(empty($argv[1]) || $argv[1] === 'help') {
	echo 'Uzycie: php bin/lab4zad1.php [nazwa pliku]'. PHP_EOL;
	echo ' gdzie:'. PHP_EOL;
	echo '  nazwa pliku zawiera sciezke do pliku CSV z ID uzytkownikow i odsluchanych przez nich utworow'. PHP_EOL;
	return;
}

echo 'Rozpoczeto wczytywanie pliku '. $argv[1] . PHP_EOL;
$startTime = microtime(true);
$loader = new Quis\Zad3\CSVLoader();
list($usersSet, $songsSet) = $loader->load($argv[1]);
$endTime = microtime(true);
$processingTime = round($endTime-$startTime, 3);
echo "Czas wczytywania pliku: $processingTime sekund". PHP_EOL;

$j = new Quis\Lab4\JaccardNeighborFinder($usersSet, 100);

$allUsers = $usersSet->getAll();
$usersCount = $usersSet->count();

$i = 0;
$uStage = 0.01*$usersCount;
$stage = 0;
$total = 0;
$startTime = microtime(true);
foreach($allUsers as $user) {
	//if(++$i > 10) break;
	
	$neighbors = $j->find($user);
	$total++;
	$stage++;
	
	if($stage >= $uStage) {
		$stage = 0;
		echo 'Sasiedzi uzytkownika '. $user->getId() .' (ilosc '. count($neighbors) .') ('. round((100*$total)/$usersCount, 2) .' %)'. PHP_EOL;		
	}
	echo 'Sasiedzi uzytkownika '. $user->getId() .' (ilosc '. count($neighbors) .'): ';
	foreach($neighbors as $id => $neighbor) {
		if($neighbor[1] > 0) {
			echo '('. $id .',j', round($neighbor[1],3) ,')';
		}
	}
	echo PHP_EOL;
}

$endTime = microtime(true);

$processingTime = round($endTime-$startTime, 2);
echo "Czas przetwarzania: $processingTime sekund". PHP_EOL;