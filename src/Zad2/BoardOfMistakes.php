<?php
namespace Quis\Zad2;

class BoardOfMistakes {
	protected $TP = 0;
	protected $FP = 0;
	protected $TN = 0;
	protected $FN = 0;
	protected $rStage;
	protected $total = 0;
	protected $stage = 0;
	protected $startTime;
	protected $endTime;
	protected $plannedTotal = 0;
	protected $mainTotalMemory = 0;
	protected $referenceTotalMemory = 0;
	
	public function __construct(int $rStage, int $plannedTotal) {
		$this->rStage = $rStage;
		$this->plannedTotal = $plannedTotal;
	}
	
	public function iterate(Set $main, Set $reference, \Iterator $iterator) {
		$this->mainTotalMemory += $main->memorySize();
		$this->referenceTotalMemory += $reference->memorySize();
		
		foreach($iterator as $i) {
			if($main->contains($i) && $reference->contains($i)) {
				$this->TP++;
			} else if(!$main->contains($i) && !$reference->contains($i)) {
				$this->TN++;
			} else if(!$main->contains($i) && $reference->contains($i)) {
				$this->FN++;
			} else if($main->contains($i) && !$reference->contains($i)) {
				$this->FP++;
			}
			$this->total++;
			$this->stage++;
			
			if($this->stage >= $this->rStage) {
				$this->stage = 0;
				echo 'Obliczanie tablicy pomylek... ('. round((100*$this->total)/$this->plannedTotal, 2) .' %)'. PHP_EOL;
			}
		}
	}
	
	public function setStartTime() {
		$this->startTime = microtime(true);
	}
	
	public function setEndTime() {
		$this->endTime = microtime(true);
	}
	
	public function output() {
		$processingTime = round($this->endTime - $this->startTime, 3);
		echo "Czas obliczania tablicy pomylek: $processingTime sekund". PHP_EOL;

		$TP = $this->TP;
		$TN = $this->TN;
		$FN = $this->FN;
		$FP = $this->FP;
		
		echo "TP = $TP" . PHP_EOL;
		if($TP+$FN > 0) {
			echo "TPR = ". round($TP/($TP+$FN), 4) . PHP_EOL;
		}
		echo "TN = $TN" . PHP_EOL;
		if($TN+$FP > 0) {
			echo "TNR = ". round($TN/($TN+$FP), 4) . PHP_EOL;
		}
		echo "FN = $FN" . PHP_EOL;
		if($TP+$FN > 0) {
			echo "FNR = ". round($FN/($TP+$FN), 4) . PHP_EOL;
		}
		echo "FP = $FP" . PHP_EOL;
		if($TN+$FP > 0) {
			echo "FPR = ". round($FP/($TN+$FP), 4) . PHP_EOL;
		}
		
		$setSize = $this->referenceTotalMemory;
		$bloomSize = $this->mainTotalMemory;
		echo "Rozmiar pamieci filtru referencyjnego: $setSize bajtow". PHP_EOL;
		echo "Rozmiar pamieci filtru Blooma: $bloomSize bajtow". PHP_EOL;
		$spared = $setSize-$bloomSize;
		$sparedPercentage = round((100*$spared)/$setSize, 3);
		echo "Zaoszczedzono $sparedPercentage % ($spared bajtow) pamieci". PHP_EOL;
	}
}