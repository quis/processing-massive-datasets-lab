<?php
namespace Quis\Zad2;

use chdemko\BitArray\BitArray;
use Quis\Zad1\DivisionHashing;
use Quis\Zad1\UniversalHashing;

class BloomFilter implements Set {
	protected $functions = [];
	protected $bitArray;
	
	public function __construct(int $functions_num, int $m, int $max) {
		$this->bitArray = BitArray::fromInteger($m);
		for($i = 0; $i < $functions_num; $i++) {
			if(!$i) {
				$this->functions[] = new DivisionHashing($m, $max);
			} else {
				$this->functions[] = new UniversalHashing($m, $max);
			}
		}
	}
	
	protected function hash(int $value): array {
		$ret = [];
		foreach($this->functions as $function) {
			$ret[] = $function->hash($value);
		}
		return $ret;
	}
	
	public function add(int $value): Set {
		$bits = $this->hash($value);
		foreach($bits as $bit) {
			$this->bitArray->offsetSet($bit, true);
		}
		return $this;
	}
	
	public function contains(int $value): bool {
		$bits = $this->hash($value);
		foreach($bits as $bit) {
			if(!$this->bitArray->offsetGet($bit)) {
				return false;
			}
		}
		return true;
	}
	
	/*
		Liczba zapalonych bitow dzielona przez 8 (brak indeksow)
	*/
	public function memorySize(): int {
		return $this->bitArray->count() / 8;
	}
}