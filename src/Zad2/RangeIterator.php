<?php
namespace Quis\Zad2;

class RangeIterator implements \Iterator {
	
	protected $max;
	protected $position;
	
	public function __construct(int $max) {
		$this->max = $max;
		$this->position = 0;
	}
	
	public function count(): int {
		return $this->max;
	}
	public function current () {
		return $this->position;
	}
	public function key() {
		return $this->position;
	}
	public function next() : void {
		++$this->position;
	}
	public function rewind() : void {
		$this->position = 0;
	}
	public function valid() : bool {
		return $this->position >= 0 && $this->position < $this->max;
	}
}