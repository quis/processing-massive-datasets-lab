<?php
namespace Quis\Zad2;

interface Set {
	public function add(int $value): Set;
	public function contains(int $value): bool;
	public function memorySize(): int;
}