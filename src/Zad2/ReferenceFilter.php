<?php
namespace Quis\Zad2;

class ReferenceFilter implements Set {
	protected $data;
	
	public function add(int $value): Set {
		$this->data[$value] = 1;
		return $this;
	}
	
	public function contains(int $value): bool {
		return isset($this->data[$value]);
	}
	
	public function size(): int {
		return count($this->data);
	}
	
	/*
		8 bajtow na jeden indeks + 1 bit na wartosc
	*/
	public function memorySize(): int {
		$count = count($this->data);
		return $count + ($count/8);
	}
}