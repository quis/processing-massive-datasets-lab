<?php
namespace Quis\Lab4;

use Quis\Zad3\Set;
use Quis\Zad3\User;

class JaccardNeighborFinder {
	protected $usersSet;
	protected $expectedNeighbors;
	
	public function __construct(Set $usersSet, int $expectedNeighbors = 100) {
		$this->usersSet = $usersSet;
		$this->expectedNeighbors = $expectedNeighbors;
	}
	
	public function find(User $user): array {
		$neighbors = [];
		$found = 0;
		
		$all = $this->usersSet->getAll();
		$songsSet = $user->getSongsSet();
		foreach($all as $otherUser) {
			if($otherUser === $user) continue;
			$intersectionCount = $this->getIntersectionCount($songsSet, $otherUser->getSongsSet());
			$unionCount = $this->getUnionCount($songsSet, $otherUser->getSongsSet());
			
			$jaccard = $intersectionCount/$unionCount;
			if($jaccard > 0) {
				$otherUserId = $otherUser->getId();
				$neighbors[$otherUserId] = [$otherUser, $jaccard];
				$found++;
			}
			
			if($found >= $this->expectedNeighbors) {
				break;
			}
		}
		
		return $neighbors;
	}
	
	protected function getIntersectionCount(Set $set1, Set $set2) {
		$count = 0;
		foreach($set1->getAll() as $val) {
			if($set2->contains($val)) {
				$count++;
			}
		}
		return $count;
	}
	
	protected function getUnionCount(Set $set1, Set $set2) {
		$count = $set1->count();
		foreach($set2->getAll() as $val) {
			if(!$set1->contains($val)) {
				$count++;
			}
		}
		return $count;
	}
}