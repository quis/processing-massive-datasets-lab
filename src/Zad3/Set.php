<?php
namespace Quis\Zad3;

class Set {
	protected $data = [];
	
	public function add(Entity $obj): Set {
		$id = $obj->getId();
		$this->data[$id] = $obj;
		return $this;
	}
	
	public function getAll(): array {
		return array_values($this->data);
	}
	
	public function contains(Entity $obj): bool {
		return $this->containsById($obj->getId());
	}
	
	public function containsById(int $entityId): bool {
		return isset($this->data[$entityId]) && $this->data[$entityId] instanceof Entity;
	}
	
	public function getById(int $entityId): ?Entity {
		return isset($this->data[$entityId]) ? $this->data[$entityId] : null;
	}
	
	public function count(): int {
		return count($this->data);
	}
	
	public function getMaxId(): ?int {
		foreach($this->data as $entity) {
			$id = $entity->getId();
			if(!isset($max) || $max < $id) {
				$max = $id;
			}
		}
		return isset($max) ? $max : null;
	}


	public function getRandomElements(int $count): array {
		$size = $this->count();
		if($count > $size) {
			throw new \Exception("Nie mozna wylosowac $count elementow ze zbioru liczacego $size elementow");
		} else if($count === $size) {
			return $this->data;
		} else {
			$ret = [];
			while(count($ret) < $count) {
				$keys = array_rand($this->data, $count);
				foreach($keys as $key) {
					$ret[$key] = $this->data[$key];
				}
			}
			return array_values($ret);
		}
	}
	public function getRandomElementsIds(int $count): array {
		$elements = $this->getRandomElements($count);
		$ret = [];
		foreach($elements as $element) {
			$ret[] = $element->getId();
		}
		return $ret;
	}
}