<?php
namespace Quis\Zad3;

use Quis\Zad1\CSVLoader as CSVLoaderFromZad1;

class CSVLoader extends CSVLoaderFromZad1 {
	public function load(string $filename, bool $debug = false): array {
		$ret = [];
		$handle = $this->validateFile($filename);

		$size = filesize($filename);
		$stage = 0;
		$total = 0;
		$usersSet = new Set();
		$songsSet = new Set();
		
		$linesize = null;
		while (($line = fgets($handle)) !== false) {
			$line = trim($line);
			if(!isset($linesize) && strlen($line) !== 0) {
				$linesize = strlen($line);
			}
			
			$lineArray = explode(',', $line);
			if(!empty($lineArray[0]) && !empty($lineArray[1]) && is_numeric($lineArray[0])) {
				foreach($lineArray as $key => $val) {
					$lineArray[$key] = intval($val);
				}
				
				if($usersSet->containsById($lineArray[0])) {
					$user = $usersSet->getById($lineArray[0]);
				} else {
					$user = new User($lineArray[0]);
					$usersSet->add($user);
				}
				if($songsSet->containsById($lineArray[1])) {
					$song = $songsSet->getById($lineArray[1]);
				} else {
					$song = new Song($lineArray[1]);
					$songsSet->add($song);
				}
				$user->addSong($song);
				
				$total++;
				$stage++;
				
				if($debug && $total > 3000) {
					break;
				} else if($stage >= 100000) {
					$stage = 0;
					if(!empty($size)) {
						$progress = round((100*$linesize*$total)/$size, 2);
						$progress_str = ($progress > 100) ? 'zaraz koniec...' : 'okolo '. $progress .' %';
					} else {
						$progress_str = 'brak danych o postepie';
					}
					echo 'Wczytano '. number_format($total, 0, ',', ' ') .' rekordow ('. $progress_str .')'.  PHP_EOL;
				}
			}
			
			
		}
		
		return [$usersSet, $songsSet];
	}
}