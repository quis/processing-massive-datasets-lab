<?php
namespace Quis\Zad3;

class User extends Entity {
	protected $songsSet;
	
	public function __construct(int $id) {
		parent::__construct($id);
		$this->songsSet = new Set();
	}
	
	public function addSong(Song $song): User {
		$this->songsSet->add($song);
		return $this;
	}
	
	public function heardSong(int $songId): bool {
		return $this->songsSet->containsById($songId);
	}
	
	public function getSongs(): array {
		return $this->songsSet->getAll();
	}
	
	public function getSongsSet(): Set {
		return $this->songsSet;
	}
	
	public function getMaxSongId(): ?int {
		return $this->songsSet->getMaxId();
	}
}