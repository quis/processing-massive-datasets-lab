<?php

namespace Quis\Zad1;

class SyntheticExperiment extends Experiment {

	const TYPE_ALL = 1;
	const TYPE_EVEN = 2;
	const TYPE_ODD = 3;
		
	public function run(HashingFunction $func, array $params) {
		echo "Rozpoczynam obliczenia dla m={$this->hashM} ". PHP_EOL;
		$resultsCount = [];
		
		if(empty($params['type'])) {
			throw new ExperimentException("Nie podano typu danych syntetycznych dla SyntheticExperiment");
		}
		
		$startTime = microtime(true);
		$maximum = $this->getMaximum();
		switch($params['type']) {
			case self::TYPE_ALL:
				for($i=0; $i <= $maximum; $i++) {
					$res = $func->hash($i);
			
					if(!isset($resultsCount[$res])) {
						$resultsCount[$res] = 1;
					} else {
						$resultsCount[$res]++;
					}
				}
				break;
			case self::TYPE_EVEN:
				for($i=0; $i <= $maximum; $i+=2) {
					$res = $func->hash($i);
			
					if(!isset($resultsCount[$res])) {
						$resultsCount[$res] = 1;
					} else {
						$resultsCount[$res]++;
					}
				}
				break;
			case self::TYPE_ODD:
				for($i=1; $i <= $maximum; $i+=2) {
					$res = $func->hash($i);
			
					if(!isset($resultsCount[$res])) {
						$resultsCount[$res] = 1;
					} else {
						$resultsCount[$res]++;
					}
				}
				break;
		}
		
		$this->outputResults($startTime, $resultsCount);
	}
}