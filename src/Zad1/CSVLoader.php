<?php
namespace Quis\Zad1;

class CSVLoader {
	public function load(string $filename, bool $debug = false): array {
		$ret = [];
		$handle = $this->validateFile($filename);

		$size = filesize($filename);
		
		$count = 0;
		$total = 0;

		$linesize = null;
		while (($line = fgets($handle)) !== false) {
			$line = trim($line);
			if(!isset($linesize) && strlen($line) !== 0) {
				$linesize = strlen($line);
			}
			$ret[] = intval(explode(',', $line)[1]);
			$count++;
			if($debug && $count > 50000) {
				break;
			} else if($count >= 100000) {
				$total += $count;
				$count = 0;
				if(!empty($size)) {
					$progress = round((100*$linesize*$total)/$size, 2);
					$progress_str = ($progress > 100) ? 'zaraz koniec...' : 'okolo '. $progress .' %';
				} else {
					$progress_str = 'brak danych o postepie';
				}
				echo 'Wczytano '. number_format($total, 0, ',', ' ') .' rekordow ('. $progress_str .')'.  PHP_EOL;
			}
		}
		return $ret;
	}
	
	protected function validateFile(string $filename) {
		if(!file_exists($filename)) {
			throw new FileLoadingException("Plik $filename nie zostal odnaleziony");
		} else {
			$handle = fopen($filename, "r");
			if(!$handle) {
				throw new FileLoadingException("Nie udalo sie otworzyc do odczytu pliku $filename");
			}
			return $handle;
		}
	}
}