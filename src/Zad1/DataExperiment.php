<?php
namespace Quis\Zad1;

class DataExperiment extends Experiment {
	
	protected function clearData(array &$data) {
		$ret = [];
		foreach($data as $record) {
			if(!is_numeric($record[1])) {
				throw new ExperimentException("Nieprawidlowe dane dla DataExperiment: proba konwersji wartosci {$record[1]} do liczby");
			}
			$ret[] = (int)$record[1];
		}
		return $ret;
	}
	
	public function calculateMaximum(array &$data) {
		$maximum = null;
		foreach($data as $record) {
			if(is_null($maximum) || $record > $maximum) {
				$maximum = $record;
			}
		}
		$this->setMaximum($maximum);
	}
	
	public function run(HashingFunction $func, array $params) {
		if(empty($params['data'])) {
			throw new ExperimentException("Nie podano danych dla DataExperiment");
		}

		$startTime = microtime(true);
		$resultsCount = [];
		foreach($params['data'] as $record) {
			$res = $func->hash($record);
			//echo 'Hash wartosci '. $record .': '. $res . PHP_EOL;
			if(!isset($resultsCount[$res])) {
				$resultsCount[$res] = 1;
			} else {
				$resultsCount[$res]++;
			}
		}
		
		$this->outputResults($startTime, $resultsCount);
	}
}