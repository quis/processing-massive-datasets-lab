<?php
namespace Quis\Zad1;

abstract class Experiment {
	protected $hashM;
	protected $maximum;
	
	public function setHashM(int $m): Experiment {
		$this->hashM = $m;
		return $this;
	}
	
	public function getHashM(): int {
		return $this->hashM;
	}
	
	public function setMaximum(int $maximum) {
		$this->maximum = $maximum;
	}
	
	public function getMaximum(): int {
		return $this->maximum;
	}
	
	protected function outputResults(int $startTime, array $resultsCount) {
		$entropy = 0.0;
		$mse = 0.0;
		$tmpM = (1/$this->getHashM());
		
		for($i = 0; $i < $this->getHashM(); $i++) {
			if(isset($resultsCount[$i])) {
				$pi = $resultsCount[$i]/$this->getMaximum();
			} else {
				$pi = 0;
			}
			if($pi !== 0) {
				$entropy += $pi * log($pi);
			}
			$mse += pow(($pi - $tmpM), 2);
		}
		$entropy = -$entropy;
		$expectedEntropy = -(log($tmpM));
		$mse = $tmpM * $mse;		
		
		$endTime = microtime(true);
		$processingTime = round($endTime-$startTime, 3);
		
		for($i = 0; $i <= 9; $i++) {
			if(!empty($resultsCount[$i])) {
				echo "Do wartosci $i zmapowano {$resultsCount[$i]} liczb". PHP_EOL;
			} else {
				echo "Do wartosci $i zmapowano 0 liczb". PHP_EOL;
			}
		}
		echo 'm = '. $this->getHashM() . PHP_EOL;
		echo "Czas przetwarzania: $processingTime sekund" . PHP_EOL;
		echo "Entropia: $entropy " . PHP_EOL;
		echo "Oczekiwana entropia: $expectedEntropy " . PHP_EOL;
		echo "Roznica: ". ($expectedEntropy - $entropy) . PHP_EOL;
		echo "Blad sredniokwadratowy (MSE): $mse " . PHP_EOL;
	}
	
	abstract public function run(HashingFunction $func, array $params);
}