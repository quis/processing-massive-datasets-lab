<?php
namespace Quis\Zad1;

class DivisionHashing extends HashingFunction {
	public function __construct(int $m, int $maximum) {
		parent::__construct($m, $maximum);
		//echo 'Utworzono obiekt DivisionHashing z parametrami: m = '. $m .', max = '. $maximum . PHP_EOL;
	}
	
	public function hash($value): int {
		return $value % $this->getM();
	}
}