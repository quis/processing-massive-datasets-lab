<?php
namespace Quis\Zad1;

class UniversalHashing extends HashingFunction {
	
	protected $a;
	protected $b;
	protected $p;
	protected static $cachedP = [];
	
	public function __construct(int $m, int $maximum) {
		parent::__construct($m, $maximum);
		
		if(static::cacheExists($maximum)) {
			$this->p = static::getFromCache($maximum);
		} else {
			$i = $maximum+1;
			while(true) {
				if($this->isPrime($i)) {
					$this->p = $i;
					break;
				}
				$i++;
				if($i % 2 === 0) {
					$i++;
				}
			}
			static::saveToCache($maximum, $this->p);
		}
		$this->a = rand(1, ($this->p)-1);
		$this->b = rand(0, ($this->p)-1);
		
		//echo 'Utworzono obiekt UniversalHashing z parametrami: a = '. $this->a .', b = '. $this->b . '. m = '. $m .', max = '. $maximum . PHP_EOL;
	}
	
	public function hash($value): int {
		return ((($this->a * $value) + $this->b) % $this->p) % $this->m;
	}
	
	protected function isPrime(int $n): bool {
		for($i = 2; $i <= ($n/2); $i++) {
			if(($n % $i) === 0) {
				return false;
			}
		}
		return true;
	}
	
	protected static function cacheExists(int $maximum): bool {
		return isset(static::$cachedP[$maximum]);
	}
	
	protected static function getFromCache(int $maximum): int {
		return static::$cachedP[$maximum];
	}
	
	protected static function saveToCache(int $maximum, int $p) {
		static::$cachedP[$maximum] = $p;
	}
}
