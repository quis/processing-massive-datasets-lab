<?php

namespace Quis\Zad1;

abstract class HashingFunction {
	protected $m;
	protected $maximum;
	
	public function __construct(int $m, int $maximum) {
		$this->m = $m;
		$this->maximum = $maximum;
	}
	
	public function getMaximumValue(): int {
		return $this->maximum;
	}
	
	public function setMaximumValue(int $maximum): int {
		$this->maximum = $maximum;
	}
	
	public function getM(): int {
		return $this->m;
	}
	
	public function setM(int $m): HashingFunction {
		$this->m = $m;
		return $this;
	}
	
	abstract public function hash($value): int;
}